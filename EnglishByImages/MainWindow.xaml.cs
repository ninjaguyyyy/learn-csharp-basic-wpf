﻿using EnglishByImages.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EnglishByImages
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Random _rng = new Random();
        private List<Vocabulary> _vocabularies = new List<Vocabulary>() {
          new Vocabulary("Nancy", "vocabulary_01.png"),
          new Vocabulary("Han Hye Jin", "vocabulary_02.png"),
          new Vocabulary("Luu Diep Phi", "vocabulary_03.jpg"),
          new Vocabulary("Trieu Man", "vocabulary_04.jpg"),
          new Vocabulary("Huong Giang", "vocabulary_05.jpg"),
          new Vocabulary("Mai Sieu Phong", "vocabulary_06.jpg"),
          new Vocabulary("Muc Niem Tu", "vocabulary_07.jpg"),
          new Vocabulary("Kang Mo-yeon", "vocabulary_08.jpg"),
          new Vocabulary("Kim Ji-won", "vocabulary_09.jpg"),
          new Vocabulary("Dien Hi cong luoc", "vocabulary_10.jpg"),
        };

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int index = _rng.Next(_vocabularies.Count);
            ImageVocabulary.Source = new BitmapImage(new Uri($"images/{_vocabularies[index].imageURL}", UriKind.Relative));
            LabelVocabulary.Content = _vocabularies[index].word;
        }
    }
}
