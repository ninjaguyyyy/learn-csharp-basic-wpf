﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnglishByImages.Models
{
    class Vocabulary
    {
        public string imageURL;
        public string word;

        public Vocabulary(string word, string imageURL)
        {
            this.imageURL = imageURL;
            this.word = word;
        }

    }
}
