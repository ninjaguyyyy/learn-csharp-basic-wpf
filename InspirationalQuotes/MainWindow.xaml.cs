﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InspirationalQuotes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Random _rng = new Random();
        private static Timer _timer;
        private const int Duration = 3000;
        private int index = 0;
        private string[] MotivationNames = new string[] { 
            "motivation_01.jpg", "motivation_02.jpg", "motivation_03.jpg", "motivation_04.jpg", "motivation_05.jpg",
            "motivation_06.jpg", "motivation_07.jpg", "motivation_08.jpg", "motivation_09.jpg", "motivation_10.jpg"
        };

        public MainWindow()
        {
            SetTimer();
            InitializeComponent();
        }

        private void SetTimer()
        {
            _timer = new Timer(Duration);
            _timer.Elapsed += OnTimedEvent;
            _timer.Start();
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            index = _rng.Next(MotivationNames.Length);
            Dispatcher.Invoke(() =>
            {
                this.ImageMotivation.Source = new BitmapImage(new Uri($"images/{MotivationNames[index]}", UriKind.Relative));
            });
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            index = _rng.Next(MotivationNames.Length);
            ImageMotivation.Source = new BitmapImage(new Uri($"images/{MotivationNames[index]}", UriKind.Relative)) ;
            
        }
    }
}
