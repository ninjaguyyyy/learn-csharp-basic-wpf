## 📺 Giới thiệu
2 phần mềm Window chức năng random ảnh -> click or timer
1. **InspirationalQuotes**  
    `Chức năng: ` Click nút Next hoặc mỗi 3s -> Random ảnh.  

    `Demo: ` <img src="https://res.cloudinary.com/dwuma83gt/image/upload/v1601481482/1_g9syrk.png" alt="ảnh demo" width="400" height="250"> 

2. **EnglishByImages**  
    `Chức năng: ` Click nút Next -> Random ảnh + từ tương ứng.  

    `Demo: ` <img src="https://res.cloudinary.com/dwuma83gt/image/upload/v1601481491/2_hkivw3.png" alt="ảnh demo" width="400" height="250">  
## 🤵 Thông tin sinh viên
`Họ tên` Nguyễn Hữu Chí  
`Mssv` 1712299  
`Email` nguyenhuuchi3006@gmail.com
## 👍 Các chức năng đã làm được
- Hiển thị ngẫu nhiên một ảnh sau khi bấm "next" hoặc mỗi 3 giây. `InspirationalQuotes`
- Hiển thị ngẫu nhiên một ảnh và một từ tương ứng sau khi bấm "next". `EnglishByImages`
## 👎 Các chức năng chưa làm được
**Không có.**
## 🎉 Các điểm đặc sắc
- Giao diện đẹp.
- Có set icon cho từng ứng dụng (thanh title window, biểu tượng dưới thanh Taskbar, file .exe)
## 💣 Các con đường bất hạnh
Không có
## 💌 Điểm đề nghị
Số điểm đề nghị: **10** điểm
## 📌 Link Youtube demo
Link: https://www.youtube.com/watch?v=jiAq5NzQfWQ